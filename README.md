# checkbox.css: Take the pain out of styling checkboxes and radios.

Anyone who's styled checkboxes and radios before knows that
it's an absolute pain to make accessible custom checkboxes and radios -
it involves a whole lot of weird hacks. Checkbox.css fixes that by
abstracting out the weird hacking, allowing you to just tweak a couple
Sass variables to style your custom checkbox. (Yes, we said .css instead of .scss,
but hey, it compiles to CSS anyway so who cares?)

## Usage ##
1. Clone this repo.
2. `npm install` (this just installs the sass compiler)
3. Make a copy of checkbox.scss.
4. Edit the variables in your copy of checkbox.scss to create your custom checkbox. You may wish to start a development server to see your changes (we include an index.html for testing out your modifications).
5. Compile it to minified CSS by using `./build.sh <your file>`.
6. Repeat steps 4 and 5 until you are satisfied.
7. You should now have a complete checkbox.css, ready to include and use!

Example HTML usage:
```html
<form>
    <div class="checkbox-group">
        <input id="checkbox1" type="checkbox" name="checkbox-test">
        <label for="checkbox1">A checkbox</label>
    </div>
    <div class="checkbox-group">
        <input id="checkbox2" type="checkbox" name="checkbox-test">
        <label for="checkbox2">Another checkbox</label>
    </div>
</form>
```
For a radio, simply change the type from "checkbox" to "radio".

NOTE: The custom checkbox depends on the existence of a `<label>` next to it
in order to work. If you do not have a label next to the checkbox, the checkbox
will not display. If you do not want a label on the checkbox,
simply use an empty label.

Read [How it Works](#how-it-works) for more info.

## Advanced ##
In case you want more control over how your checkbox is displayed, you can
manually override the ::before and ::after pseudo-elements to create
custom checkbox areas and checkmark icons. Selectors for these are included
at the bottom of checkbox.scss.

Example SCSS (overrides default checkbox area border):
```scss
.checkbox-group input[type="checkbox"] + label::before {
    border: 1px solid #000;
}
```
The full list of available selectors is contained in checkbox.scss.

## How it Works ##
TL;DR checkbox.css abstracts a common hack used to make a custom checkbox:
1. Create a div container with your checkbox and a label.
2. Hide the default checkbox.
3. Create a ::before CSS pseudo-element on the label and position it.
This becomes our checkbox area.
4. Create a ::after CSS pseudo-element on the label and position it over the ::before.
This becomes our checkmark.
5. Style the ::before and ::after elements to respond to :focus, :hover and :checked
selectors on the hidden checkbox input. (Although it's hidden, users can still
interact with it; this is how we make our custom checkbox work).

Because the custom checkbox depends on having a label to attach ::before and ::after
pseudo-elements to, if you don't have a label it won't be able to show the checkbox.
This can be worked around by just using an empty label.

Although it isn't *too* hard to do this without the help of checkbox.css, it's a pain
to do by hand, which is why checkbox.css abstracts out most of the hacking
and instead lets you focus on styling the actual checkbox.
